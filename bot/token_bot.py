from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
import time
# Make file for Keys
import keys as K

driver = webdriver.Chrome()

# ------Enter Attendance Token-------#
driver.get("https://sis.galvanize.com/cohorts/17/attendance/mine/")


begin_button = driver.find_element(by=By.CLASS_NAME, value="button")
print(begin_button.text)
begin_button.click()
time.sleep(2)
user_email = driver.find_element(by=By.NAME, value="user[email]")
user_password = driver.find_element(by=By.NAME, value="user[password]")
signin = driver.find_element(by=By.NAME, value="commit")
user_email.send_keys(K.SIS_EMAIL)
user_password.send_keys(K.SIS_PASS)
signin.click()
time.sleep(2)
count = 0
max_count = 120
while count < max_count:
    time.sleep(.5)
    try:
        token = driver.find_element(by=By.CLASS_NAME, value="tag")
        count = max_count
    except NoSuchElementException:
        count += 1
        driver.refresh()
    print(count)

token_input = driver.find_element(by=By.NAME, value="token")
token = driver.find_element(by=By.CLASS_NAME, value="tag")
token_input.send_keys(f"{token.text}")
begin_button = driver.find_element(by=By.CLASS_NAME, value="button")
begin_button.click()
driver.quit()



print("success")
